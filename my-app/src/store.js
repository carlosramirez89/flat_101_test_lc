import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import productsReducer from './reducers/productsReducer';

const store = createStore(
    productsReducer,
    compose(applyMiddleware(thunk),
    typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined' ? 
        window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
    )
);

export default store;