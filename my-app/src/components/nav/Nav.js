import React from 'react';
import {NavLink} from 'react-router-dom';
import './Nav.scss';

const Nav = () => {
    return (
        <nav className="menu">
            <ul>
                <li><NavLink to="/products">Listado de productos</NavLink></li>
                <li><NavLink to="/create">Crear producto</NavLink></li>
            </ul>
        </nav>
    );
}
 
export default Nav;