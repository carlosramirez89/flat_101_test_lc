import React, { useEffect } from 'react';
import './Productos.scss';
import { useSelector, useDispatch } from 'react-redux';
import { getProducts } from '../../actions/productsActions';
import Error from '../error/Error';
import PopMessage from '../popMessage/PopMessage';
import Paginator from './paginator/Paginator';
import Select from './select/Select';
import ProductMenu from './product-menu/ProductMenu';

const Productos = () => {
    const products = useSelector(state => state.products);
    const dispatch = useDispatch();

    useEffect(() => {
        const loadUsers = () => dispatch(getProducts())
        loadUsers();
    }, [])

    const handleClick = (e) => {
        if(e.target.classList.contains('clicked')) {
            e.target.classList = 'heart icon-heart-o';
        } else {
            e.target.classList = 'clicked heart icon-heart1';
        }
    }

    return (
        <div className="wrapper-products">
            <PopMessage/>
            <ProductMenu/>
            <h4>123 Items found</h4>
            <h1>Search Results for "Bathroom taps"</h1>
            <Select/>
            {products ? products.map((product, index) => {
                return (<div key={index} className="product-card">
                    <div className="wrapper-img">
                        <img src={product.url} alt={product.title} />
                    </div>
                    <h3>{product.title}</h3>
                    <p>{product.price}</p>
                    <i onClick={(e) => handleClick(e)} className="heart icon-heart-o"></i>
                </div>
                )
            }) : <Error />}
            <Paginator/>
        </div>

    );
}

export default Productos;