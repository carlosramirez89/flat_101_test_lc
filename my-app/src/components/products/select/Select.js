import React from 'react';
import './Select.scss';

const Select = () => {

    const handleClick = (e) => {
        if(e.target.parentElement.classList.contains('open')) {
            e.target.parentElement.classList.remove('open');
        } else {
            e.target.parentElement.classList.add('open');
        }
    }
    
    return (
        <div className="wrapper-select">
            <div className="select">
                <ul className="products">
                    <li onClick={(e) => handleClick(e)}> Productos (53) <i className="icon-keyboard_arrow_down"></i>
                        <ul>
                            <li>Waterfall</li>
                            <li>Modern</li>
                            <li>Belmont</li>
                            <li>Emperador</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div className="select">
                <ul className="order">
                    <li onClick={(e) => handleClick(e)}>ORDENAR POR <i className="icon-keyboard_arrow_down"></i>
                        <ul>
                            <li>Precio</li>
                            <li>Nombre</li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>

    );
}
 
export default Select;