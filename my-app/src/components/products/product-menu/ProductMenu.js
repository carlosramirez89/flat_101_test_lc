import React from 'react';
import './ProductMenu.scss';

const ProductMenu = () => {
    return (
        <div className="products-menu">
            <div className="close-tag"><i className="icon-cross"></i></div>
            <div className="inner-menu">
                <i className="icon-user"></i>
                <i className="icon-search"></i>
            </div>
        </div>
    );
}

export default ProductMenu;