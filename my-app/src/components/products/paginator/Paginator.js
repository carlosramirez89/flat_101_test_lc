import React from 'react';
import './Paginator.scss';

const Paginator = () => {
    return (
        <div className="paginator">
            <ul>
                <li className="active">1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
                <li className="arrow"><i className="icon-arrow-right2"></i></li>
            </ul>
        </div>
    );
}
 
export default Paginator;