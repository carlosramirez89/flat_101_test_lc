import React, {useState} from 'react';
import { Fragment } from 'react';
import './create.scss';
import { useDispatch } from 'react-redux';
import { newProduct } from '../../actions/productsActions';
import { useHistory } from 'react-router';

const CreateNew = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const [product, setProduct] = useState({
        title: "",
        price: "",
        url: "",
    });

    const [error, setError] = useState('');

    const { title, price, url } = product;

    const updateInput = e => {
        setProduct({
            ...product,
            [e.target.name]: e.target.value,
        });
    }

    const sendForm = (e) => {
        e.preventDefault();

        if (title=== "") {
            setError('El producto tiene que tener algún titulo');
        } else if (price === "") {
            setError('El precio no puede estar vacío');
        } else if (url === "") {
            setError('El producto necesita una URL');
        } else {
            dispatch(newProduct(product));
            history.push('/');
        }
    }

    return (
        <Fragment>
            {error !== '' ? <p className="error-message">{error}</p> : null}
            <form className="form-create" onSubmit={sendForm}>
                <h2>Crear nuevo producto</h2>
                <div className="form-group">
                    <label>Nombre del producto</label>
                    <input 
                        type="text" 
                        name="title"
                        onInput={updateInput}
                        placeholder="Grifo moderno"
                    />
                </div>
                <div className="form-group">
                    <label>Precio</label>
                    <input 
                        type="text" 
                        name="price"
                        onInput={updateInput}
                        placeholder="44€"
                    />
                </div>
                <div className="form-group">
                    <label>Imagen URL</label>
                    <input 
                        type="text" 
                        name="url"
                        onInput={updateInput}
                        placeholder="https://blog.gala.es/wp-content/uploads/2019/12/tipos-de-grifos-monomando-1024x614.jpg"
                    />
                </div>
                <button type="submit">Crear producto</button>
            </form>
        </Fragment>
    );
}
 
export default CreateNew;