import React from 'react';
import { useSelector} from 'react-redux';
import './PopMessage.scss';

const PopMessage = () => {

    const message = useSelector(state => state.message);

    return (
        <div className={message ? 'pop-message show': 'pop-message'}>
            {message}
        </div>
    );
}
 
export default PopMessage;