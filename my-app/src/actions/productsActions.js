import {GETPRODUCTS, GETPRODUCTSFAIL, NEWPRODUCT} from '../types';
import axios from 'axios';

export function getProducts() {
    return async (dispatch) => {
        try {
            const response = await axios.get('http://localhost:4000/api/productos');
            dispatch(getProductsSucceed(response.data))
        }

        catch (error) {
            dispatch(getProductsFail(error))
        }
    }
}

const getProductsSucceed = (products) => ({
    type: GETPRODUCTS,
    payload: products

});

const getProductsFail  = (error) => ({
    type: GETPRODUCTSFAIL,
    payload: error
});


export function newProduct(product) {
    return async (dispatch) => {
        try {
            const response = await axios.post('http://localhost:4000/api/productos/new-product', product);
            dispatch(newProductSucceed(response.data))
        }

        catch (error) {
            dispatch(getProductsFail(error))
        }
    }
}

const newProductSucceed = (message) => ({
    type: NEWPRODUCT,
    payload: message
});