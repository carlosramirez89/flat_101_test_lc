import './App.scss';
import './icon-font/icomoon.scss';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import CreateNew from './components/create/createNew';
import Products from './components/products/Productos';
import Nav from './components/nav/Nav';
import {Provider} from 'react-redux';
import store from './store';

function App() {
  return (
    <div className="App">
      <Router>
        <Nav/>
        <Provider store={store}>
        <Switch>
          <Route exact path="/" component={Products}/>
          <Route path="/products" component={Products}/>
          <Route path="/create" component={CreateNew}/>
        </Switch>
        </Provider>
      </Router>
    </div>
  );
}

export default App;
