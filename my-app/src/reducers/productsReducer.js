import {GETPRODUCTS, GETPRODUCTSFAIL, NEWPRODUCT} from '../types';


const initialState = {
    products: null,
    loading:false,
    message:null
}

const productsReducer = function(state = initialState, action) {
    switch(action.type) {

        case GETPRODUCTS:
            return({
                ...state,
                products: action.payload,
            })

        case GETPRODUCTSFAIL:
            return({
                products: null
            })

        case NEWPRODUCT:
            return({
                ...state,
                message:action.payload
            })

        default :      
            return state;

    }
}

export default productsReducer;